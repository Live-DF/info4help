#!/bin/bash
# update translation script
# info4help
echo ""
echo "générer le .pot"
echo ""
/usr/lib/python3.5/Tools/i18n/pygettext.py -o locale/info4help.pot lib/cmd.py lib/export.py
/usr/lib/python3.5/Tools/i18n/pygettext.py -o locale/info4help-cli.pot info4help-cli ui/cli.py
/usr/lib/python3.5/Tools/i18n/pygettext.py -o locale/info4help-web.pot info4help-web ui/web.py

cd locale
echo ""
echo "mise à jour des fichiers de traduction"
echo ""
msgmerge --update fr/LC_MESSAGES/info4help.po info4help.pot
msgmerge --update fr/LC_MESSAGES/info4help-cli.po info4help-cli.pot
msgmerge --update fr/LC_MESSAGES/info4help-web.po info4help-web.pot
echo ""
echo "suppression des .mo obsolètes"
echo ""
find . -name "info4help.mo" -exec rm {} \;
find . -name "info4help-cli.mo" -exec rm {} \;
find . -name "info4help-web.mo" -exec rm {} \;
echo ""
echo "édition des fichiers : @toi de jouer. reviens et lance le script de génération des .mo"
echo -n "[Enter] pour quitter"
read anykey
exit 0

