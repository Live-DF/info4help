#!/bin/bash
# création de .mo
# info4help
echo ""
echo "Suppression des anciens fichiers *.po"
echo ""
find locale -name "info4help.po~" -exec rm {} \;
find locale -name "info4help-cli.po~" -exec rm {} \;
find locale -name "info4help-web.po~" -exec rm {} \;
echo ""
echo "Génération des .mo"
echo ""
cd locale/fr/LC_MESSAGES
msgfmt info4help.po -o info4help.mo
msgfmt info4help-cli.po -o info4help-cli.mo
msgfmt info4help-web.po -o info4help-web.mo
echo ""
echo -n ".mo générés ! [Enter] pous quitter"
read anykey
exit 0

