��          �   %   �      0  >   1     p  =   }     �     �  C   �     )  4   I     ~  D   �  ^   �     9     H  '   O  _   w     �  K   �     3     P     l     �     �     �  w  �  M   8     �  Y   �     �        ?   $     d  +   �  #   �  M   �  �   "	     �	     �	  %   �	  ]   �	     P
  V   c
  *   �
     �
                <     ]                                      
                                                                     	                        -t <file> or --template=<file> -> Template file for export  <arguments> According to your choice, your session password may be asked. Argument list: Confirm with the Enter key Copy this URL address below which links to your report on internet: Export template not available!  Please find below the report to copy/paste on forums Press Enter to exit... The answer is pre-formatted, you can easily copy/paste it on forums. This application obtains information about your Debian GNU/Linux system to get help on forums. Unknown choice Usage: What information are you interested in? Would you like upload the report on internet and obtain the URL address ? (on a pastebin) [y/n] Wrong argument! You can also provide the pastebin link (if asked): it points on the report. all information listed above information on the hardware information on the network information on the storage information on the system yes Project-Id-Version: info 4help
POT-Creation-Date: 2016-09-30 16:26+0200
PO-Revision-Date: 2016-09-30 16:28+0200
Last-Translator: Sylvain Treutenaere <starsheep@openmailbox.org>
Language-Team: French
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
     -t <fichier> ou --template=<fichier> -> Fichier de template pour l'export  <arguments> Selon votre choix, le mot de passe superadministrateur (root) pourra vous être demandé. Liste des arguments : Validez votre choix avec Entrée Copiez ce lien qui contient toutes les informations ci-dessus : Template d'export non valide !  Voici le code à copier/coller sur le forum Appuyez sur Entrée pour quitter... Le texte sera pré-formaté, il vous suffit de le copier/coller sur le forum. Cette page vous permettra de transmettre des informations à propos de votre système Debian GNU/Linux pour obtenir de l'aide sur les forums. Choix inconnu Utilisation : Quelles informations souhaitez vous ? Voulez vous coller ces informations en ligne et récupérer le lien ? (sur un pastebin) [o/N] Mauvais argument ! Vous pouvez aussi donner le lien qui vous sera donné : il pointe vers le compte-rendu toutes les informations listées ci-dessus informations sur le matériel informations sur le réseau informations sur les disques informations sur la distribution oui 