��          �            x  >   y     �     �     �     �                    )     1  j   9     �     �     �  V   �  w  -  M   �     �                &  	   F     P     `     m     u  �   ~          #     1  b   D                                    	                
                               -t <file> or --template=<file> -> Template file for export  <arguments> Argument list: Complete report Export template not available!  Hardware Info4df report Installation Network Storage The following infomation are already formatted. All you need to do is to copy/paste in your forum message. Upload on paste.debian.net Usage: Wrong argument! You can also upload this inofmation on a pastebin by clicking on the following button: Project-Id-Version: info 4help
POT-Creation-Date: 2016-09-30 16:26+0200
PO-Revision-Date: 2016-09-30 16:28+0200
Last-Translator: Sylvain Treutenaere <starsheep@openmailbox.org>
Language-Team: French
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
     -t <fichier> ou --template=<fichier> -> Fichier de template pour l'export  <arguments> Liste des arguments : Rapport complet Template d'export non valide !  Matériel Rapport info4df Installation Réseau Stockage Les informations ci-dessous sont déjà pré-formatées. Il ne vous reste plus qu'à les copier/coller dans votre message sur le forum. Envoyer sur paste.debian.net Utilisation : Mauvais argument ! Vous pouvez aussi envoyer le rapport complet sur le pastebin de debian en cliquant sur ce bouton : 