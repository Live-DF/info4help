DESTDIR =
INFODIR = /usr/share/info4help
APPDIR = /usr/share/applications
ICONDIR = /usr/share/pixmaps
MANDIR = /usr/share/man/man1
FRDIR = /usr/share/locale/fr/LC_MESSAGES
POLDIR = /usr/share/polkit-1/actions

help:
	@echo "Usage: as root"
	@echo "make install  : installs application"
	@echo "make uninstall: uninstalls application"

install:
	#mise en place des dossiers
	install -d -m 755 -o root -g root ${DESTDIR}${BINDIR}
	install -d -m 755 -o root -g root ${DESTDIR}${INFODIR}
	install -d -m 755 -o root -g root ${DESTDIR}${APPDIR}
	install -d -m 755 -o root -g root ${DESTDIR}${ICONDIR}
	install -d -m 755 -o root -g root ${DESTDIR}${MANDIR}
	install -d -m 755 -o root -g root ${DESTDIR}${FRDIR}
	install -d -m 755 -o root -g root ${DESTDIR}${POLDIR}
	#mise en place des icons
	install -m 644 -o root -g root *.png ${DESTDIR}${ICONDIR}
	#mise en place des scripts
	install -m 755 -o root -g root info4help-cli ${DESTDIR}${INFODIR}
	install -m 755 -o root -g root info4help-web ${DESTDIR}${INFODIR}
	#mise en place des lib,scripts & templates
	cp -R lib/ ${DESTDIR}${INFODIR}
	cp -R scripts/ ${DESTDIR}${INFODIR}
	cp -R templates/ ${DESTDIR}${INFODIR}
	cp -R ui/ ${DESTDIR}${INFODIR}
	cp -R ui/libweb/ ${DESTDIR}${INFODIR}/ui
	#mise en place des lanceurs
	install -m 644 -o root -g root *.desktop ${DESTDIR}${APPDIR}
	#mise en place des mans
	install -m 644 -o root -g root *.1 ${DESTDIR}${MANDIR}
	#polkit
	install -m 644 -o root -g root polkit-actions/*.policy ${DESTDIR}${POLDIR}
	#trads
	install -m 644 -o root -g root locale/fr/LC_MESSAGES/*.mo ${DESTDIR}${POLDIR}
	#clean git
	find ${DESTDIR}${INFODIR} -name ".gitignore" -exec rm {} \;

uninstall:
	rm ${DESTDIR}${ICONDIR}/info4help-cli.png
	rm ${DESTDIR}${ICONDIR}/info4help-web.png
	rm -Rf ${DESTDIR}${INFODIR}
	rm ${DESTDIR}${APPDIR}/info4help-cli.desktop
	rm ${DESTDIR}${APPDIR}/info4help-web.desktop
	rm ${DESTDIR}${MANDIR}/info4help-cli.1
	rm ${DESTDIR}${MANDIR}/info4help-web.1
	rm ${DESTDIR}${POLDIR}/org.info4help.policykit.info4help_root.policy
