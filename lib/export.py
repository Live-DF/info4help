#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""
Developers :                 thuban (thuban@yeuxdelibad.net)
                             arpinux (arpinux@member.fsf.org)
                             Starsheep (starsheep@openmailbox.org)
                             FirePowi
                             Michael Gebetsroither <michael@mgeb.org> (paste part)
                             
Licence :            GNU General Public Licence v3

Description :           Tool to list a description of the system
                        to improve help on forums in case of trouble.
"""


import sys
import gettext
if sys.version[0] == "2":
        from urllib import urlencode
        from urllib import FancyURLopener
else:
        from urllib.parse import urlencode
        from urllib.request import FancyURLopener
        
appname="info4help"

#i18n
gettext.bindtextdomain(appname, '/usr/share/locale')
gettext.textdomain(appname)
_ = gettext.gettext


def safe_unicode(obj, *args):
    """ return the unicode representation of obj """
    try:
        return unicode(obj, *args)
    except UnicodeDecodeError:
        # obj is byte string
        ascii_text = str(obj).encode('string_escape')
        return unicode(ascii_text)
        

def upload_code(code):
        """upload code to paste.debian.net
        return the page url or False
        """
        DEFAULT_SERVER='https://paste.debian.net/'
        
        user = "DFrocks"
        expire = -1
        lang = "text"
        private = 0
        
        params = { "poster":user, "expire":expire, "lang":lang, "private":private, "code":code.encode('utf-8') }
        params = urlencode(params)

        url_opener = FancyURLopener()
        try:
            page = url_opener.open(DEFAULT_SERVER, params)
            #page_url = page.read().decode('utf-8').strip()
            return(page.url)
        except Exception as e:
            print(_("Failed to uplad code to pastebin: \n{}").format(e)) 
            return(False)


def paste_code(code):
    paste_url = upload_code(code)
    if paste_url:
        import webbrowser
        webbrowser.open(paste_url)
        return(paste_url)
        

def export(file_, title, cmd, cmdtype, code, codetype=""):
    template = []
    with open(file_, "r") as f:
        for ligne in f.readlines():
            template.append(ligne)
    if len(code) > 0:
        out = template[0].format(title)+"\n"
        out += template[2].format(cmdtype,cmd)+"\n"
        if codetype == "":
            out += template[1].format(code)+"\n"
        else:
            out += template[2].format(codetype,code)+"\n"
        if sys.version[0] == "2":
            out = safe_unicode(out)
    else:
        out = ""
    return(out)
 
